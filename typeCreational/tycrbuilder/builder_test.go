package tycrbuilder_test

import (
	printr "fmt"
	"log"
	"testing"

	builder "designpatternsingo/typeCreational/tycrbuilder"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing builder test , review Code and behavior")
	printr.Println()

	normalBuilder := builder.GetBuilder("normal")
	iglooBuilder := builder.GetBuilder("igloo")
	director := builder.NewDirector(normalBuilder)
	normalHouse := director.BuildHouse()

	printr.Printf("Normal House Door Type: %s\n", normalHouse.DoorType)
	printr.Printf("Normal House Window Type: %s\n", normalHouse.WindowType)
	printr.Printf("Normal House Num Floor: %d\n", normalHouse.Floor)

	director.SetBuilder(iglooBuilder)
	iglooHouse := director.BuildHouse()

	printr.Printf("\nIgloo House Door Type: %s\n", iglooHouse.DoorType)
	printr.Printf("Igloo House Window Type: %s\n", iglooHouse.WindowType)
	printr.Printf("Igloo House Num Floor: %d\n", iglooHouse.Floor)

	printr.Println()
	log.Println("Finish builder test , review Code and behavior")
}
