package tycrprototype

import (
	printer "fmt"
)

type Folder struct {
	Childrens []Inode
	Name      string
}

func (f *Folder) Print(indentation string) {
	printer.Println(indentation + f.Name)

	for _, i := range f.Childrens {
		i.Print(indentation + indentation)
	}
}

func (f *Folder) Clone() Inode {
	cloneFolder := &Folder{}
	cloneFolder.Name = f.Name + "_clone"
	tempChildrens := []Inode{}

	for _, i := range f.Childrens {
		thecopy := i.Clone()
		tempChildrens = append(tempChildrens, thecopy)
	}

	cloneFolder.Childrens = tempChildrens

	return cloneFolder
}
