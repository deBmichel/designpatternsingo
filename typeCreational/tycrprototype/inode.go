package tycrprototype

type Inode interface {
	Print(string)
	Clone() Inode
}
