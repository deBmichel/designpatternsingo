package tycrprototype_test

import (
	prntr "fmt"
	"log"
	"testing"

	prttp "designpatternsingo/typeCreational/tycrprototype"
)

func TestPrototype(t *testing.T) {
	t.Parallel()

	log.Println("Executing test to pattern prototype, review behavior")
	prntr.Println()

	file1 := &prttp.File{Name: "File1"}
	file2 := &prttp.File{Name: "File2"}
	file3 := &prttp.File{Name: "File3"}
	folder1 := &prttp.Folder{
		Childrens: []prttp.Inode{file1},
		Name:      "Folder1",
	}
	folder2 := &prttp.Folder{
		Childrens: []prttp.Inode{folder1, file2, file3},
		Name:      "Folder2",
	}

	prntr.Println("Printing hierarchy for Folder2")
	folder2.Print("  ")
	cloneFolder := folder2.Clone()

	log.Println("\nPrinting hierarchy for clone Folder")
	cloneFolder.Print("  ")
	prntr.Println()
	log.Println("test to pattern prototype has finished review behavior")
}
