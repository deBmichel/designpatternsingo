package tycrprototype

import (
	printer "fmt"
)

type File struct {
	Name string
}

func (f *File) Print(indentation string) {
	printer.Println(indentation + f.Name + "_clone")
}

func (f *File) Clone() Inode {
	return &File{Name: f.Name + "_clone"}
}
