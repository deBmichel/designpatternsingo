package tycrfactory_test

import (
	prtntr "fmt"
	"log"
	"testing"

	factory "designpatternsingo/typeCreational/tycrfactory"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing factory test , review Code and behavior")
	prtntr.Println()

	ak47, _ := factory.GetGun()("ak47")
	maverick, _ := factory.GetGun()("maverick")

	factory.PrintDetails()(ak47)
	factory.PrintDetails()(maverick)

	prtntr.Println()
	log.Println("Finish factory test , review Code and behavior")
}
