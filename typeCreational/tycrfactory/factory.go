package tycrfactory

import (
	"errors"
	printer "fmt"
)

var ErrWrongGunType = errors.New("wrong gun type passed")

type iGun interface {
	setName(name string)
	setPower(power int)
	getName() string
	getPower() int
}

type gun struct {
	name  string
	power int
}

func (g *gun) setName(name string) {
	g.name = name
}

func (g *gun) getName() string {
	return g.name
}

func (g *gun) setPower(power int) {
	g.power = power
}

func (g *gun) getPower() int {
	return g.power
}

type ak47 struct {
	gun
}

func newAk47() iGun {
	ak47power := 4

	return &ak47{
		gun: gun{
			name:  "AK47 gun",
			power: ak47power,
		},
	}
}

type maverick struct {
	gun
}

func newMaverick() iGun {
	maverickpower := 4

	return &maverick{
		gun: gun{
			name:  "Maverick gun",
			power: maverickpower,
		},
	}
}

func getGun(gunType string) (iGun, error) {
	if gunType == "ak47" {
		return newAk47(), nil
	}

	if gunType == "maverick" {
		return newMaverick(), nil
	}

	return nil, ErrWrongGunType
}

func printDetails(g iGun) {
	printer.Printf("Gun: %s", g.getName())
	printer.Println()
	printer.Printf("Power: %d", g.getPower())
	printer.Println()
}

func GetGun() func(string) (iGun, error) {
	return getGun
}

func PrintDetails() func(iGun) {
	return printDetails
}
