package tycrabstractfactory

type Nike struct{}

const NikeSize = 14

func (n *Nike) MakeShoe() Iproduct {
	return &NikeShoe{
		Shoe: Shoe{
			Logo: "nike",
			Size: NikeSize,
		},
	}
}

func (n *Nike) MakeShort() Iproduct {
	return &NikeShort{
		Short: Short{
			Logo: "nike",
			Size: NikeSize,
		},
	}
}
