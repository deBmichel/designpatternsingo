package tycrabstractfactory_test

import (
	prntter "fmt"
	"log"
	"testing"

	abstrcfctry "designpatternsingo/typeCreational/tycrabstractfactory"
)

func TestAbstrcfctry(t *testing.T) {
	t.Parallel()
	log.Println("Executing abstractfactory test , review Code and behavior")
	prntter.Println()

	adidasFactory, _ := abstrcfctry.GetSportsFactory("adidas")
	nikeFactory, _ := abstrcfctry.GetSportsFactory("nike")
	nikeShoe := nikeFactory.MakeShoe()
	nikeShort := nikeFactory.MakeShort()
	adidasShoe := adidasFactory.MakeShoe()
	adidasShort := adidasFactory.MakeShort()

	abstrcfctry.PrintShoeDetails(nikeShoe)
	abstrcfctry.PrintShortDetails(nikeShort)
	abstrcfctry.PrintShoeDetails(adidasShoe)
	abstrcfctry.PrintShortDetails(adidasShort)

	prntter.Println()
	log.Println("Finish abstractfactory test , review Code and behavior")
}
