package tycrabstractfactory

import (
	printr "fmt"
)

type Short struct {
	Logo string
	Size int
}

func (s *Short) SetLogo(logo string) {
	s.Logo = logo
}

func (s *Short) GetLogo() string {
	return s.Logo
}

func (s *Short) SetSize(size int) {
	s.Size = size
}

func (s *Short) GetSize() int {
	return s.Size
}

func PrintShortDetails(s Iproduct) {
	printr.Printf("Logo: %s", s.GetLogo())
	printr.Println()
	printr.Printf("Size: %d", s.GetSize())
	printr.Println()
}
