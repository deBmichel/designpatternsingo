package tycrabstractfactory

import (
	"errors"
)

var ErrWrongType = errors.New("wrong brand type passed")

type ISportsFactory interface {
	MakeShoe() Iproduct
	MakeShort() Iproduct
}

func GetSportsFactory(brand string) (ISportsFactory, error) {
	if brand == "adidas" {
		return &Adidas{}, nil
	}

	if brand == "nike" {
		return &Nike{}, nil
	}

	return nil, ErrWrongType
}
