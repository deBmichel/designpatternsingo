package tycrabstractfactory

import (
	printr "fmt"
)

type Shoe struct {
	Logo string
	Size int
}

func (s *Shoe) SetLogo(logo string) {
	s.Logo = logo
}

func (s *Shoe) GetLogo() string {
	return s.Logo
}

func (s *Shoe) SetSize(size int) {
	s.Size = size
}

func (s *Shoe) GetSize() int {
	return s.Size
}

func PrintShoeDetails(s Iproduct) {
	printr.Printf("Logo: %s", s.GetLogo())
	printr.Println()
	printr.Printf("Size: %d", s.GetSize())
	printr.Println()
}
