package tycrabstractfactory

type Adidas struct{}

const AdidasSize = 17

func (a *Adidas) MakeShoe() Iproduct {
	return &AdidasShoe{
		Shoe: Shoe{
			Logo: "adidas",
			Size: AdidasSize,
		},
	}
}

func (a *Adidas) MakeShort() Iproduct {
	return &AdidasShort{
		Short: Short{
			Logo: "adidas",
			Size: AdidasSize,
		},
	}
}
