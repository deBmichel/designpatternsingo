package tycrobjectpool_test

import (
	prntr "fmt"
	"log"
	"strconv"
	"testing"

	opdp "designpatternsingo/typeCreational/tycrobjectpool"
)

func TestPrototype(t *testing.T) {
	t.Parallel()

	log.Println("Executing test to pattern objectpool, review behavior")
	prntr.Println()
	log.Println("Testing two connections")

	connections := make([]opdp.IPoolObject, 0)

	for i := 0; i < 3; i++ {
		c := &opdp.Connection{ID: strconv.Itoa(i)}
		connections = append(connections, c)
	}

	pool, err := opdp.InitPool(connections)
	if err != nil {
		log.Fatalf("Init Pool Error: %s", err)
	}

	conn1, err := pool.Loan()
	if err != nil {
		log.Fatalf("Pool Loan Error: %s", err)
	}

	conn2, err := pool.Loan()
	if err != nil {
		log.Fatalf("Pool Loan Error: %s", err)
	}

	err = pool.Receive(conn1)
	if err != nil {
		log.Println(err)
	}

	err = pool.Receive(conn2)
	if err != nil {
		log.Println(err)
	}

	prntr.Println()
	log.Println("test to pattern objectpool has finished review behavior")
}
