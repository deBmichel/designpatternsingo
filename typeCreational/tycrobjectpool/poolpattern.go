package tycrobjectpool

import (
	"errors"
	prntr "fmt"
	"sync"
)

var (
	ErrPoolZeroLength            = errors.New("cannot craete a pool of 0 length")
	ErrNoFreePoolObjcts          = errors.New("no pool object free. Please request after sometime")
	ErrTargetObjectDoesnotBelong = errors.New("target pool object doesn't belong to the pool")
)

type IPoolObject interface {
	getID() string // This is any id which can be used to compare two different pool objects
}

type Pool struct {
	idle     []IPoolObject
	active   []IPoolObject
	capacity int
	mulock   *sync.Mutex
}

// InitPool Initialize the pool.
func InitPool(poolObjects []IPoolObject) (*Pool, error) {
	if len(poolObjects) == 0 {
		return nil, ErrPoolZeroLength
	}

	active := make([]IPoolObject, 0)
	pool := &Pool{
		idle:     poolObjects,
		active:   active,
		capacity: len(poolObjects),
		mulock:   new(sync.Mutex),
	}

	return pool, nil
}

func (p *Pool) Loan() (IPoolObject, error) {
	p.mulock.Lock()
	defer p.mulock.Unlock()

	if len(p.idle) == 0 {
		return nil, ErrNoFreePoolObjcts
	}

	obj := p.idle[0]
	p.idle = p.idle[1:]
	p.active = append(p.active, obj)

	prntr.Printf("Loan Pool Object with ID: %s\n", obj.getID())

	return obj, nil
}

func (p *Pool) Receive(target IPoolObject) error {
	p.mulock.Lock()
	defer p.mulock.Unlock()

	err := p.remove(target)
	if err != nil {
		return err
	}

	p.idle = append(p.idle, target)

	prntr.Printf("Return Pool Object with ID: %s\n", target.getID())

	return nil
}

func (p *Pool) remove(target IPoolObject) error {
	currentActiveLength := len(p.active)

	for i, obj := range p.active {
		if obj.getID() == target.getID() {
			p.active[currentActiveLength-1], p.active[i] = p.active[i], p.active[currentActiveLength-1]
			p.active = p.active[:currentActiveLength-1]

			return nil
		}
	}

	return ErrTargetObjectDoesnotBelong
}

type Connection struct {
	ID string
}

func (c *Connection) getID() string {
	return c.ID
}
