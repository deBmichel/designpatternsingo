package tycrsingleton

import (
	"log"
	"os"
	"sync"
)

const iterfilelog = "iterlogs.log"

// The Basic logger struct with unexported fields.(warning, info, error).
type TheIterLogger struct {
	warningLogger *log.Logger
	infoLogger    *log.Logger
	errorLogger   *log.Logger
}

// Method StartTheIterLogger to Basic logger struct.
// Init is not a great practice in golang.
func (l *TheIterLogger) StartTheIterLogger() {
	log.Println("Preparing logger instance")

	file, err := os.OpenFile(iterfilelog, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	// Be quiet houze with the file log, if the file doesn't exist create it.

	l.infoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	l.warningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	l.errorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

// Add a warning group of args in the logger.
func (l *TheIterLogger) LogWarning(args ...interface{}) {
	l.warningLogger.Println(args...)
}

// Add a LogInfo group of args in the logger.
func (l *TheIterLogger) LogInfo(args ...interface{}) {
	l.infoLogger.Println(args...)
}

// Add a LogInfo group of args in the logger.
func (l *TheIterLogger) LogError(args ...interface{}) {
	l.errorLogger.Println(args...)
}

var (
	// ErrPttrnImsingletoninstance The unique instance of logger; is named error to pass linter
	// Global vars are not a great practice in golang.
	ErrPttrnImsingletoninstance *TheIterLogger
	// ErrPttrnImsingletononcer The unique sync.Once to create unique instance of logger.
	ErrPttrnImsingletononcer sync.Once
)

// Create one instance of loger one time.
// Return the unique instance of logger.
func Logger() *TheIterLogger {
	ErrPttrnImsingletononcer.Do(func() {
		log.Println("Creating one instance of logger threat safe..")
		ErrPttrnImsingletoninstance = &TheIterLogger{}
		ErrPttrnImsingletoninstance.StartTheIterLogger()
	})

	return ErrPttrnImsingletoninstance
}
