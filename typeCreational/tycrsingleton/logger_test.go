package tycrsingleton_test

import (
	prntr "fmt"
	"log"
	"testing"

	singleton "designpatternsingo/typeCreational/tycrsingleton"
)

func TestLogger(t *testing.T) {
	t.Parallel()

	getterLogerInstance := func(finish chan bool, times int) {
		for i := 0; i < times; i++ {
			singleton.Logger().LogWarning("TESTING LogWarning")
			singleton.Logger().LogInfo("TESTING LogInfo")
			singleton.Logger().LogError("TESTING LogError")
		}
		log.Println("Logger() was used", times, "times")
		finish <- true
	}

	finished := make(chan bool)

	log.Println(">>>>> Calling 10 times getLoggerInstance, review behavior")
	prntr.Println()

	go getterLogerInstance(finished, 10)
	<-finished

	prntr.Println()
	log.Println("<<<<< Finished, you should see and review file iterlogs.log")
}
