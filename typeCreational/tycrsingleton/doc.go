/*package singletondt is an example of implementation of de design pattern
singleton in golang.

Singleton Design Pattern is a creational design pattern and also one of the most
commonly used design pattern. This pattern is used when only a single instance of
the struct should exist. This single instance is called a singleton object. Some
of the cases where the singleton object is applicable:

DB instance – we only want to create only one instance of DB object and that
instance will be used throughout the application.
Logger instance – again only one instance of the logger should be created and it
should be used throughout the application.

The singleton instance is created when the struct is first initialized.  Usually,
there is getInstance() method defined on the struct for which only one instance
needs to be created. Once created then the same singleton instance is returned every
time by the getInstance().

definition source:
	https://golangbyexample.com/singleton-design-pattern-go/

In this package you are going to find logger.go; an example of singleton in a logger
instance.

Read the code, check the sources, think how you could do it differently
and the initial code without changes you can test it by executing the
command go test inside the folder of the pattern type.*/
package tycrsingleton
