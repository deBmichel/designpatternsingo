<div align="center">
	<h1>Design Patterns In Golang</h1>
	<img src="repsources/imagedpt.png" alt="Header custom image"/>
</div>

It is a minor and simple study about design patterns in Golang taken from the full 
source https://golangbyexample.com/all-design-patterns-golang/, my study contains 
functional code aviable to run vía go test in the respective packages, with fixes
and changes from the original.

You should be carefull, And review with calm the implementation, I wrote the code-study,
and I understand what I was doing, but in patterns you need extra knowledges, learn
a little bit more about dessing patterns and then read the code; you could find variables
that should not be exported, I apologize for that but this study was lightning.

While doing this study, I found to my surprise a source about patterns in Golang 
written in Spanish and with a really very professional content, the author of that 
content is Daniel M Spiridione and the source address is:
http://www.designpatternsingo.com/. You will find a very deep and detailed explanation 
about Golang patterns in that source, for sure.

<h1> Intellectual property and acknowledgments. </h1>

Please review the doc vía **go doc package** , in the doc are the references of the sources 
and the intellectual origin of the code that I used for my study( Additionally with the
original sources you could do a line by line comparison and notice the differences or 
changes and with that you should come up with questions and maybe solving those questions 
will allow you to learn new things).
I do not know Daniel or the authors of https://golangbyexample.com/all-design-patterns-golang/
, but I really thank you for doing that job. I could not have done a good study or a good 
apprenticeship without their work. Respected Daniel and unknown authors, my study has a lot of
content from the work that you did and whoever reads this will know.

<h1> Reading go doc and testing the code.</h1>

This is a simple study but done with the purpose of learning something about patterns and 
having functional code of each learned pattern. Inside each pattern package you can run the 
go doc "NamePackage" to learn something about the pattern and the full sources and you can
execute the respective go test in the package.

Fast Example:

**cd tystdecorator**

**go doc tystdecorator**

**go test**

You should find something like :

<div align="center">
	<img src="repsources/example.png" alt="Header custom image"/>
	<br>
	<h1> 
		Thanks for visiting this repo and best wishes for success in your 
		code and projects. 
	</h1>	
</div>

