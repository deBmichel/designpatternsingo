package tystdecorator

const tomatotoppingprice = 7

type TomatoTopping struct {
	ActrPizza pizza
}

func (c *TomatoTopping) GetPrice() int {
	pizzaPrice := c.ActrPizza.GetPrice()

	return pizzaPrice + tomatotoppingprice
}
