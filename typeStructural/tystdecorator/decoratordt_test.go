package tystdecorator_test

import (
	tprintr "fmt"
	"log"
	"testing"

	dcrtr "designpatternsingo/typeStructural/tystdecorator"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("executing decorator test , review Code and behavior")
	tprintr.Println()

	veggiePizza := &dcrtr.VeggeMania{}

	tprintr.Printf("adding cheese topping to veggiePizza\n")

	veggiePizzaWithCheese := &dcrtr.CheeseTopping{
		ActrPizza: veggiePizza,
	}

	tprintr.Printf("adding tomato topping to veggiePizza\n")

	withCheeseAndTomato := &dcrtr.TomatoTopping{
		ActrPizza: veggiePizzaWithCheese,
	}

	tprintr.Printf("price of veggieMania pizza with tomato and cheese topping is %d\n", withCheeseAndTomato.GetPrice())
	tprintr.Printf("veggiePizza final state %d\n", veggiePizza.GetPrice())

	peppyPaneerPizza := &dcrtr.PeppyPaneer{}

	tprintr.Printf("add cheese topping to peppyPaneerPizza\n")

	peppyPaneerPizzaWithCheese := &dcrtr.CheeseTopping{
		ActrPizza: peppyPaneerPizza,
	}
	tprintr.Printf("price of peppyPaneer with cheese topping is %d\n", peppyPaneerPizzaWithCheese.GetPrice())
	tprintr.Printf("peppyPaneerPizza final state %d\n", peppyPaneerPizza.GetPrice())
	tprintr.Println()
	log.Println("finish decorator test , review Code and behavior")
}
