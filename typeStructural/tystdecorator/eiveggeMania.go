package tystdecorator

const veggeManiaPrice = 15

type VeggeMania struct{}

func (p *VeggeMania) GetPrice() int {
	return veggeManiaPrice
}
