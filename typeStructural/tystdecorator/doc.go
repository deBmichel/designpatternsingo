/*Decorator design pattern is a structural design pattern. It lets
you provide additional functionality or decorates an object without
altering that object.

Taken from the full source:
	https://golangbyexample.com/decorator-pattern-golang/

Read the code, check the sources, think how you could do it differently
and the initial code without changes you can test it by executing the
command go test inside the folder of the pattern type.
*/
package tystdecorator
