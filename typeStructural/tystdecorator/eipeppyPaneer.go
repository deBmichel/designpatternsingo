package tystdecorator

const peppyPaneerPrice = 30

type PeppyPaneer struct{}

func (p *PeppyPaneer) GetPrice() int {
	return peppyPaneerPrice
}
