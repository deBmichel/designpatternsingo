package tystdecorator

const cheesetopingprice = 10

type CheeseTopping struct {
	ActrPizza pizza
}

func (c *CheeseTopping) GetPrice() int {
	pizzaPrice := c.ActrPizza.GetPrice()

	return pizzaPrice + cheesetopingprice
}
