package tystpdproxy_test

import (
	tprintr "fmt"
	"log"
	"testing"

	prx "designpatternsingo/typeStructural/tystpdproxy"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing proxy test , review Code and behavior")
	tprintr.Println()

	navegadorProxy := &prx.BrowserProxy{Browser: &prx.Browser{}}
	tprintr.Printf("%s\n", navegadorProxy.Direction("http://google.com"))
	tprintr.Printf("%s\n", navegadorProxy.Direction("http://twitter.com"))
	tprintr.Printf("%s\n", navegadorProxy.Direction("http://facebook.com"))

	tprintr.Println()
	log.Println("Finish proxy test , review Code and behavior")
}
