package tystpdproxy

type BrowserProxy struct {
	Browser BrowserInterface
}

func (n *BrowserProxy) Direction(url string) string {
	if url == "http://twitter.com" || url == "http://facebook.com" {
		return "Access DENEGATED TO " + url
	}

	return n.Browser.Direction(url)
}
