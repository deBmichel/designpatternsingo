package tystfacade

import (
	prntr "fmt"
)

type notification struct{}

func (n *notification) sendWalletCreditNotification() {
	prntr.Println("Sending wallet credit notification")
}

func (n *notification) sendWalletDebitNotification() {
	prntr.Println("Sending wallet debit notification")
}
