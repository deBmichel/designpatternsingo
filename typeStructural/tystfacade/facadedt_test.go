package tystfacade_test

import (
	tprintr "fmt"
	"log"
	"testing"

	fcd "designpatternsingo/typeStructural/tystfacade"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing facade test , review Code and behavior")
	tprintr.Println()

	walletFacade := fcd.NewWalletFacade("abc", 1234)

	tprintr.Println()

	err := walletFacade.AddMoneyToWallet("abc", 1234, 10)
	if err != nil {
		log.Fatalf("Error: %s\n", err.Error())
	}

	tprintr.Println()

	err = walletFacade.DeductMoneyFromWallet("abc", 1234, 5)
	if err != nil {
		log.Fatalf("Error: %s\n", err.Error())
	}

	tprintr.Println()
	log.Println("Finish facade test , review Code and behavior")
}
