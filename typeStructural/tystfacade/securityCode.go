package tystfacade

import (
	"errors"
	prntr "fmt"
)

var ErrSecurityCode = errors.New("security Code is incorrect")

type securityCode struct {
	code int
}

func newSecurityCode(code int) *securityCode {
	return &securityCode{
		code: code,
	}
}

func (s *securityCode) checkCode(incomingCode int) error {
	if s.code != incomingCode {
		return ErrSecurityCode
	}

	prntr.Println("SecurityCode Verified")

	return nil
}
