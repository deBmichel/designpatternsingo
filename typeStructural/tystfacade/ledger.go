package tystfacade

import (
	prntr "fmt"
)

type ledger struct{}

func (s *ledger) makeEntry(accountID, txnType string, amount int) {
	prntr.Printf("make ledger entry for accountId %s with txnType %s for amount %d\n", accountID, txnType, amount)
}
