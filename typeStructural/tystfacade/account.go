package tystfacade

import (
	"errors"
	prntr "fmt"
)

var ErrAccntNmErr = errors.New("account Name is incorrect")

type account struct {
	name string
}

func newAccount(accountName string) *account {
	return &account{
		name: accountName,
	}
}

func (a *account) checkAccount(accountName string) error {
	if a.name != accountName {
		return ErrAccntNmErr
	}

	prntr.Println("Account Verified")

	return nil
}
