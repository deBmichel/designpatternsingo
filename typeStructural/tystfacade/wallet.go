package tystfacade

import (
	"errors"
	prntr "fmt"
)

var ErrInsufficentBalance = errors.New("balance is not sufficient")

type wallet struct {
	balance int
}

func newWallet() *wallet {
	return &wallet{
		balance: 0,
	}
}

func (w *wallet) creditBalance(amount int) {
	w.balance += amount

	prntr.Println("Wallet balance added successfully")
}

func (w *wallet) debitBalance(amount int) error {
	if w.balance < amount {
		return ErrInsufficentBalance
	}

	prntr.Println("Wallet balance is Sufficient")

	w.balance -= amount

	return nil
}
