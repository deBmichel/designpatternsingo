package tystflyweight

type ScreenFactory struct {
	controls []Control
}

func (pb *ScreenFactory) ObtainControl(thetype string, reference string, x string, y string) string {
	for _, control := range pb.controls {
		if control.GetReference() == reference {
			return control.Draw(x, y) + " || <control retaken>"
		}
	}

	if thetype == "BOTON" {
		control := &Boton{reference}

		pb.controls = append(pb.controls, control)

		return control.Draw(x, y)
	}

	if thetype == "PASSWORD" {
		control := &FieldPassword{}

		return control.Draw(x, y)
	}

	return ""
}
