package tystflyweight_test

import (
	tprintr "fmt"
	"log"
	"testing"

	flywghtdt "designpatternsingo/typeStructural/tystflyweight"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing flyweightdt test , review Code and behavior")
	tprintr.Println()

	screen := &flywghtdt.ScreenFactory{}
	tprintr.Printf("%s\n", screen.ObtainControl("BOTON", "BTN1", "100", "300"))
	tprintr.Printf("%s\n", screen.ObtainControl("BOTON", "BTN2", "200", "300"))
	tprintr.Printf("%s\n", screen.ObtainControl("BOTON", "BTN3", "300", "300"))
	tprintr.Printf("%s\n", screen.ObtainControl("PASSWORD", "PWD1", "500", "300"))
	tprintr.Printf("%s\n", screen.ObtainControl("BOTON", "BTN1", "400", "300"))

	tprintr.Println()
	log.Println("Finish flyweightdt test , review Code and behavior")
}
