/*It is a structural design pattern. This pattern is used when a
large number of similar objects need to be created. These objects
are called flyweight objects and are immutable.

Let’s first see an example. Flyweight Pattern will be clear after
this example.

Taken from the full source:
	https://golangbyexample.com/flyweight-design-pattern-golang/

Review : Intrinsic and Extrinsic States

When to Use:
When the objects have some intrinsic properties which can be shared.

Use flyweight when a large number of objects needs to be created which
can cause memory issue. In case figure out all the common or intrinsic
state and create flyweight objects for that.

Example taken from the interesting source:
	http://www.designpatternsingo.com/#patron_flyweight*/
package tystflyweight
