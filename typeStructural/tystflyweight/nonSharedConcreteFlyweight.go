package tystflyweight

type FieldPassword struct{}

func (cp *FieldPassword) Draw(x string, y string) string {
	return "Drawing FIELD Password in " + x + ", " + y
}

func (cp *FieldPassword) GetReference() string {
	return ""
}
