package tystflyweight

type Boton struct {
	reference string
}

func (b *Boton) Draw(x string, y string) string {
	return "Drawing Botón #" + b.reference + " in " + x + ", " + y
}

func (b *Boton) GetReference() string {
	return b.reference
}
