package tystflyweight

type Control interface {
	Draw(x string, y string) string
	GetReference() string
}
