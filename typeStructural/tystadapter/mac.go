package tystadapter

import (
	printer "fmt"
)

type Mac struct{}

func (m *Mac) InsertInSquarePort() {
	printer.Println("Insert square port into mac machine")
}
