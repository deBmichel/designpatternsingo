/*Convert the interface of a class into another interface clients expect.
An adapter lets classes work together that could not otherwise because of
incompatible interfaces. The enterprise integration pattern equivalent is
the translator.
taken from :
	https://en.wikipedia.org/wiki/Software_design_pattern

Problem:

We have a class (Client) that is expecting some features of an object
(square USB port here), but we have another object called adaptee (Windows
Laptop here) which offers the same functionality but through a different
interface( circular port)

This is where Adapter Pattern comes into the picture. We create a class
known as Adapter that will:
	- Adhere to the same interface which client expects ( Square USB port
	  here)
	- Translate the request from the client to the adaptee in the form that
	  adaptee expects. Basically, in our example act as an adapter that
	  accepts USB in square port and then inserts into circular port in
	  windows laptop.

When to Use:
Use this design pattern when the objects implement a different interface
as required by the client.

taken from the full source:
	https://golangbyexample.com/adapter-design-pattern-go/

Read the code, check the sources, think how you could do it differently
and the initial code without changes you can test it by executing the
command go test inside the folder of the pattern type.*/
package tystadapter
