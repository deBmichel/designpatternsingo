package tystadapter_test

import (
	printr "fmt"
	"log"
	"testing"

	adptr "designpatternsingo/typeStructural/tystadapter"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing adapter test , review Code and behavior")
	printr.Println()

	client := &adptr.Client{}
	mac := &adptr.Mac{}

	client.InsertSquareUsbInComputer(mac)

	windowsMachine := &adptr.Windows{}
	windowsMachineAdapter := &adptr.WindowsAdapter{
		WindowMachine: windowsMachine,
	}
	client.InsertSquareUsbInComputer(windowsMachineAdapter)

	printr.Println()
	log.Println("Finish adapter test , review Code and behavior")
}
