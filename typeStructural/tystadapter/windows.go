package tystadapter

import (
	printer "fmt"
)

type Windows struct{}

func (w *Windows) InsertInCirclePort() {
	printer.Println("Insert circle port into windows machine")
}
