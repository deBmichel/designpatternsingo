package tystcomposite_test

import (
	prntr "fmt"
	"log"
	"testing"

	cmpstdt "designpatternsingo/typeStructural/tystcomposite"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing composite test , review Code and behavior")
	prntr.Println()

	file1 := &cmpstdt.File{}
	file1.Name = "File1"
	file2 := &cmpstdt.File{}
	file2.Name = "File2"
	file3 := &cmpstdt.File{}
	file3.Name = "File3"
	folder1 := &cmpstdt.Folder{}
	folder1.Name = "Folder1"
	folder1.Add(file1)

	folder2 := &cmpstdt.Folder{}
	folder2.Name = "Folder2"
	folder2.Add(file2)
	folder2.Add(file3)
	folder2.Add(folder1)
	folder2.Search("rose")

	prntr.Println()
	log.Println("Finish composite test , review Code and behavior")
}
