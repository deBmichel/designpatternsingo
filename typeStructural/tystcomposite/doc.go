/*This is a structural design pattern. Composition design pattern is
used when we want a Group of objects called ‘composite’ is treated
in a similar way as a single object. It comes under structural
design pattern as it allows you to compose objects into a tree
structure. Each of the individual objects in the tree structure can
be treated in the same way irrespective of whether they are Complex
or Primitive.

Let’s try to understand it with an example of a file system of OS.
In the file system, there are two types of objects File and Folder.
There are cases when File and Folder are treated to be the same way.
It will be more clear as we go along.

When to Use
Composite Design pattern makes sense to use in cases when the composite
and individual object needs to be treated in the same way from a client
perspective.

    – In our example above of the file system, let’s say search operation
    of a particular keyword needs to be executed. Now, this search operation
    applies to both File and Folder. For a File, it will just look into the
    contents of the file and for a Folder, it will go through all files in
    the hierarchy in that folder to find that keyword

Use this pattern when the composite and individual object form a tree-like structure
    –  In our example, File and Folder do form a tree structure

Taken from the full source:
	https://golangbyexample.com/composite-design-pattern-golang/

Read the code, check the sources, think how you could do it differently
and the initial code without changes you can test it by executing the
command go test inside the folder of the pattern type.
*/
package tystcomposite
