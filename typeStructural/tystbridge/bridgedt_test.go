package tystbridge_test

import (
	sprinter "fmt"
	"log"
	"testing"

	bridge "designpatternsingo/typeStructural/tystbridge"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing bridge test , review Code and behavior")
	sprinter.Println()

	hpPrinter := &bridge.Hp{}
	epsonPrinter := &bridge.Epson{}
	macComputer := &bridge.Mac{}
	macComputer.SetPrinter(hpPrinter)
	macComputer.Print()
	sprinter.Println()
	macComputer.SetPrinter(epsonPrinter)
	macComputer.Print()
	sprinter.Println()

	winComputer := &bridge.Windows{}
	winComputer.SetPrinter(hpPrinter)
	winComputer.Print()
	sprinter.Println()
	winComputer.SetPrinter(epsonPrinter)
	winComputer.Print()
	sprinter.Println()

	log.Println("Finish bridge test , review Code and behavior")
}
