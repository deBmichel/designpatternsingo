/*Bridge design pattern is a structural design pattern that allows
the separation of abstraction from its implementation. Sounds
confusing? Don’t worry, it will be more clear as we go along.

This pattern suggests dividing a large class into two separate hierarchy

Abstraction – It is an interface and children of the Abstraction are
referred to as Refined Abstraction. The abstraction contains a reference
to the implementation.
Implementation – It is also an interface and children’s of the Implementation
are referred to as Concrete Implementation

Abstraction hierarchy is being referred to by clients without worrying about
the implementation. Let’s take an example. Assume you have two types of computer
mac and windows. Also, let’s say two types of printer epson and hp . Both
computers and printers needs to work with each other in any combination.
The client will only access the computer without worrying about how print is
happening. Instead of creating four structs for the 2*2 combination, we create
two hierarchies

Taken from the full source:
	https://golangbyexample.com/bridge-design-pattern-in-go/

Read the code, check the sources, think how you could do it differently
and the initial code without changes you can test it by executing the
command go test inside the folder of the pattern type.
*/
package tystbridge
