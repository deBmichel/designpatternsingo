package tystbridge

import (
	printer "fmt"
)

type Epson struct{}

func (p *Epson) PrintFile() {
	printer.Println("Printing by a EPSON Printer")
}
