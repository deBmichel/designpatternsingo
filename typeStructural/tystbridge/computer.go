package tystbridge

type Computer interface {
	Print()
	SetPrinter(Printer)
}
