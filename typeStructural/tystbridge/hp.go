package tystbridge

import (
	printr "fmt"
)

type Hp struct{}

func (p *Hp) PrintFile() {
	printr.Println("Printing by a HP Printer")
}
