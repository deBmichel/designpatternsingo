package tystbridge

import (
	printr "fmt"
)

type Mac struct {
	Printer Printer
}

func (m *Mac) Print() {
	printr.Println("Print request for mac")
	m.Printer.PrintFile()
}

func (m *Mac) SetPrinter(p Printer) {
	m.Printer = p
}
